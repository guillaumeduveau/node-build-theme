![coverage](https://gitlab.com/guillaumeduveau/docker-node-build-theme/badges/master/build.svg)

This very simple image, based on Alpine, is meant to build websites themes with Node.js :

+ nodejs
+ npm
+ bower
+ grunt
+ gulp
