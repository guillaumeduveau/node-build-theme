FROM alpine:3.6

LABEL "com.example.vendor"="Guillaume Duveau" \
      description="Node.js with Bower, Gulp and & Grunt."

# Install Node
RUN apk add --no-cache \
            nodejs \
            nodejs-npm

# Install Bower, Gulp and Grunt
RUN npm install -g \
                bower \
                gulp-cli \
                grunt-cli

# Allow Bower to run as root
RUN echo '{ "allow_root": true }' > /root/.bowerrc

# Define working directory.
WORKDIR /data

# Define default command.
CMD ["bash"]
